# Configuration values

# this is displayed in the page title
site_name = 'Codewiz'

# this is used for the leftmost button of the navbar.
# It can be set to an empty string or a text message
site_icon = 'sys/favicon16x16.png'

# set to None for read-only sites, or
# leave empty ('') to allow anonymous edits
# otherwise, set to a URL that requires authentication
privileged_url = 'https://www.codewiz.org/wikiedit'
#privileged_url = ''
#privileged_url = None

data_dir = 'data'

# default page links
meta_urls = [
    # http-equiv         content
    [ 'X-XRDS-Location', '/yadis.xml' ],
]
link_urls = [
    # rel                href
    [ 'icon',            'sys/favicon.gif' ],
    [ 'stylesheet',      'sys/geekigeeki.css' ],
    [ 'openid.server',   'https://id.codewiz.org/bernie' ],
    [ 'openid.delegate', 'https://id.codewiz.org/bernie' ],
    [ 'pavatar',         '/wiki/BernieAvatar80x80.png' ],
]

history_url = '../gitweb/wiki.git'

post_edit_hook = os.path.join(script_path(), 'post_edit_hook.sh')

datetime_fmt = '%a, %d %b %Y %H:%M:%S %Z'

# prefix before nonexistent link (usually '?')
nonexist_pfx = ''

# maximum image width in pixels for thumbnails (set to '' to disable scaling)
image_maxwidth = 512

# Set to True for CGI var dump
debug_cgi = False
