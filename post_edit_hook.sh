#!/bin/sh

# Die easily and make sure the cgi collects stderr too
set -e
exec 2>&1

if [ $# -ne 4 ]; then
	echo "Usage $0 <path-to-commit> <remote-user> <remote-host>"
	exit 1
fi

repo=`dirname "$1"`
file=`basename "$1"`

cd "$repo"
git add "$file" # In case it's a new page

GIT_COMMITTER_NAME='GeekiGeeki' \
GIT_COMMITTER_EMAIL='webmaster@codewiz.org' \
git commit -a -m "$4" --author="$2 <$2@$3>"
