#bernie

# path to git projects (<project>.git)
$projectroot = "/srv/www-codewiz/wiki/git";

@git_base_url_list = (
	'git://codewiz.org/~bernie',
	'http://codewiz.org/~bernie/git',
	'ssh://shell.codewiz.org/~bernie/public_git');

$site_name   = "Codewiz Git Repositories";

# templating
@stylesheets = ("../wiki/gitweb.css");
$logo        = "../wiki/git-logo.png";
$favicon     = "../wiki/favicon.ico";

$feature{'blame'}{'default'} = [1];

#$feature{'avatar'}{'default'} = ['gravatar'];

#bernie: not working with rewrite rule
$feature{'pathinfo'}{'default'} = [1];

#bernie: disabled because lame spam harvesters download them all
#$feature{'snapshot'}{'default'} = ['tgz', 'tbz2', 'zip'];
