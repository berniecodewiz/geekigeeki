#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 1999, 2000 Martin Pool <mbp@humbug.org.au>
# Copyright (C) 2002 Gerardo Poggiali
# Copyright (C) 2007, 2008, 2009, 2010, 2011 Bernie Innocenti <bernie@codewiz.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__version__ = '4.0-' + '$Id$'[4:11]

from time import clock, localtime, gmtime, strftime
start_time = clock()
title_done = False

import cgi, sys, os, re, errno, stat, glob

image_ext = 'png|gif|jpg|jpeg|svg|bmp|ico'
video_ext = 'avi|webm|mkv|ogv'
image_re = re.compile(r".*\.(" + image_ext + ")$", re.IGNORECASE)
video_re = re.compile(r".*\.(" + video_ext + ")$", re.IGNORECASE)
# FIXME: we accept stuff like foo/../bar and we shouldn't
file_re  = re.compile(r"([A-Za-z0-9_\-][A-Za-z0-9_\.\-/ ]*)$")
url_re   = re.compile(r"[a-z]{3,8}://[^\s'\"]+\S$")
ext_re   = re.compile(r"\.([^\./]+)$")

def config_get(key, default=None):
    return globals().get(key, default)

def script_name():
    return os.environ.get('SCRIPT_NAME', '')

#TODO: move post-edit hook into wiki, then kill this
def script_path():
    return os.path.split(os.environ.get('SCRIPT_FILENAME', ''))[0]

def query_string():
    path_info = os.environ.get('PATH_INFO', '')
    if len(path_info) and path_info[0] == '/':
        return path_info[1:] or 'FrontPage'
    else:
        return os.environ.get('QUERY_STRING', '') or 'FrontPage'

def is_privileged():
    purl = config_get('privileged_url')
    return (purl is not None) and os.environ.get('SCRIPT_URI', '').startswith(purl)

def remote_user():
    user = os.environ.get('REMOTE_USER', '')
    if user is None or user == '' or user == 'anonymous':
        user = 'AnonymousCoward'
    return user

def remote_host():
    return os.environ.get('REMOTE_ADDR', '')

def get_hostname(addr):
    try:
        from socket import gethostbyaddr
        return gethostbyaddr(addr)[0] + ' (' + addr + ')'
    except Exception:
        return addr

def is_external_url(pathname):
    return (url_re.match(pathname) or pathname.startswith('/'))

def relative_url(pathname, privileged=False):
    if not is_external_url(pathname):
        if privileged:
            url = config_get('privileged_url') or script_name()
        else:
            url = script_name()
        pathname = url + '/' + pathname
    return cgi.escape(pathname, quote=True)

def permalink(s):
    return re.sub(' ', '-', re.sub('[^a-z0-9_ ]', '', s.lower()).strip())

def humanlink(s):
    return re.sub(r'(?:.*[/:]|)([^:/\.]+)(?:\.[^/:]+|)$', r'\1', s.replace('_', ' '))

# Split arg lists like "blah|blah blah| width=100 | align = center",
# return a list containing anonymous arguments and a map containing the named arguments
def parse_args(s):
    args = []
    kvargs = {}
    for arg in s.strip('<[{}]>').split('|'):
        m = re.match('\s*(\w+)\s*=\s*(.+)\s*', arg)
        if m is not None:
            kvargs[m.group(1)] = m.group(2)
        else:
            args.append(arg.strip())
    return (args, kvargs)

def url_args(kvargs):
    argv = []
    for k, v in kvargs.items():
        argv.append(k + '=' + v)
    if argv:
        return '?' + '&amp;'.join(argv)
    return ''

def emit_header(mtime=None, mime_type="text/html"):
    if mtime:
        # Prevent caching when the wiki engine gets updated
        mtime = max(mtime, os.stat(__file__).st_mtime)
        print("Last-Modified: " + strftime("%a, %d %b %Y %H:%M:%S GMT", gmtime(mtime)))
    else:
        print("Cache-Control: must-revalidate, max-age=0")
    print("Content-type: " + mime_type + "; charset=utf-8")
    print('')

def send_guru(msg_text, msg_type):
    if not msg_text: return
    print('<pre id="guru" onclick="this.style.display = \'none\'" class="' + msg_type + '">')
    if msg_type == 'error':
        print('    Software Failure.  Press left mouse button to continue.\n')
    print(cgi.escape(msg_text))
    if msg_type == 'error':
        print '\n           Guru Meditation #DEADBEEF.ABADC0DE'
    print('</pre><script type="text/javascript" src="%s" defer="defer"></script>' \
        % relative_url('sys/GuruMeditation.js'))

def send_httperror(status="404 Not Found", query="", trace=False):
    print("Status: %s" % status)
    msg_text = "%s: on query '%s'" % (status, query)
    if trace:
        import traceback
        msg_text += '\n\n' + traceback.format_exc()
    page = Page()
    page.send_title(msg_text=msg_text)
    page.send_footer()

def link_tag(dest, text=None, privileged=False, **kvargs):
    if text is None:
        text = humanlink(dest)
    elif image_re.match(text):
        text = '<img style="border: 0" src="' + relative_url(text) + '" alt="' + text + '" />'

    link_class = kvargs.get('class', kvargs.get('cssclass', None))
    if not link_class:
        if is_external_url(dest):
            link_class = 'external'
        elif file_re.match(dest) and Page(dest).exists():
            link_class = 'wikilink'
        else:
            text = config_get('nonexist_pfx', '') + text
            link_class = 'nonexistent'

    # Prevent crawlers from following links potentially added by spammers and to autogenerated pages
    nofollow = ''
    if link_class in ('external', 'navlink', 'nonexistent'):
        nofollow = 'rel="nofollow" '

    return '<a class="%s" %shref="%s">%s</a>' % (link_class, nofollow, relative_url(dest, privileged=privileged), text)

def link_inline(name, descr=None, kvargs={}):
    if not descr: descr = humanlink(name)
    url = relative_url(name)
    if video_re.match(name):
        args = ''
        if 'maxwidth' in kvargs:
            args += 'width=' + kvargs['maxwidth']
        return '<video controls="1" src="%s" %s/>' % (url, args)
    elif image_re.match(name):
        return '<a href="%s"><img border="0" src="%s" alt="%s" /></a>' % (url, url + url_args(kvargs), descr)
    elif file_re.match(name) and not ext_re.search(name): # FIXME: this guesses a wiki page
        Page(name).send_naked(kvargs) # FIXME: we should return the page as a string rather than print it
        return ''
    else:
        return '<iframe width="100%%" scrolling="auto" frameborder="0" src="%s"><a href="%s">%s</a></iframe>' \
            % (url, url, name)

def link_inline_glob(pattern, descr=None, kvargs={}):
    if not url_re.match(pattern) and bool(set(pattern) & set('?*[')):
        s = ''
        for name in sorted(glob.glob(pattern), reverse=bool(int(kvargs.get('reverse', '0'))) ):
            s += link_inline(name, descr, kvargs)
        return s
    else:
        return link_inline(pattern, descr, kvargs)

def search_stats(hits, searched):
    return "%d hits out of %d pages searched.\n" % (hits, searched)

def handle_fullsearch(query, form):
    needle = form['q'].value
    Page().send_title(text='Full text search for "' + needle + '"')

    needle_re = re.compile(needle, re.IGNORECASE)
    hits = []
    all_pages = page_list()
    for page_name in all_pages:
        body = Page(page_name).get_raw_body()
        count = len(needle_re.findall(body))
        if count:
            hits.append((count, page_name))

    # The default comparison for tuples compares elements in order, so this sorts by number of hits
    hits.sort()
    hits.reverse()

    out = ''
    for (count, page_name) in hits:
        out += ' * [[' + page_name + ']] . . . ' + str(count) + ' ' + ['match', 'matches'][count != 1] + '\n'

    out += search_stats(len(hits), len(all_pages))
    WikiFormatter(out).print_html()

def handle_titlesearch(query, form):
    needle = form['q'].value
    Page().send_title(text='Title search for "' + needle + '"')

    needle_re = re.compile(needle, re.IGNORECASE)
    all_pages = page_list()
    hits = list(filter(needle_re.search, all_pages))

    out = ''
    for filename in hits:
        out += ' * [[' + filename + ']]\n'

    out += search_stats(len(hits), len(all_pages))
    WikiFormatter(out).print_html()

def handle_raw(pagename, form):
    Page(pagename).send_raw()

def handle_atom(pagename, form):
    Page(pagename).send_atom()

def handle_edit(pagename, form):
    pg  = Page(pagename)
    if 'save' in form:
        if form['file'].value:
            pg.save(form['file'].file.read(), form['changelog'].value)
        else:
            pg.save(form['savetext'].value.replace('\r\n', '\n'), form['changelog'].value)
        pg.send()
    elif 'cancel' in form:
        pg.msg_text = 'Editing canceled'
        pg.msg_type = 'notice'
        pg.send()
    else: # preview or edit
        text = None
        if 'preview' in form:
            text = form['savetext'].value
        pg.send_editor(text)

def handle_get(pagename, form):
    if not ext_re.search(pagename): # FIXME: no extension guesses a wiki page
        Page(pagename).send()
    else:
        # FIMXE: this is all bullshit, MimeTypes bases its guess on the extension!
        from mimetypes import MimeTypes
        mimetype, encoding = MimeTypes().guess_type(pagename)
        Page(pagename).send_raw(mimetype=mimetype, args=form)

# Used by sys/macros/WordIndex and sys/macros/TitleIndex
def make_index_key():
    links = ['<a href="#%s">%s</a>' % (ch, ch) for ch in 'abcdefghijklmnopqrstuvwxyz']
    return '<p style="text-align: center">' + ' | '.join(links) + '</p>'

def page_list(dirname=None, search_re=None):
    if search_re is None:
        # FIXME: WikiWord is too restrictive now!
        search_re = re.compile(r"^\b((([A-Z][a-z0-9]+){2,}/)*([A-Z][a-z0-9]+){2,})\b$")
    return sorted(filter(search_re.match, os.listdir(dirname or '.')))

def _macro_ELAPSED_TIME(*args, **kvargs):
    return "%03f" % (clock() - start_time)

def _macro_VERSION(*args, **kvargs):
    return __version__

class WikiFormatter:
    """Object that turns Wiki markup into HTML."""
    def __init__(self, raw, kvargs=None):
        self.raw = raw
        self.kvargs = kvargs or {}
        self.h_level = 0
        self.in_pre = self.in_html = self.in_table = self.in_li = False
        self.in_header = True
        self.list_indents = [] # a list of pairs (indent_level, list_type) to track nested lists
        self.tr_cnt = 0
        self.styles = {
            #wiki   html   enabled?
            "//":  ["em",  False],
            "**":  ["b",   False],
            "##":  ["tt",  False],
            "__":  ["u",   False],
            "--":  ["del", False],
            "^^":  ["sup", False],
            ",,":  ["sub", False],
            "''":  ["em",  False], # LEGACY
            "'''": ["b",   False], # LEGACY
        }

    def _b_repl(self, word):
        style = self.styles[word]
        style[1] = not style[1]
        return ['</', '<'][style[1]] + style[0] + '>'

    def _glyph_repl(self, word):
        return '&mdash;'

    def _tit_repl(self, word):
        link = permalink(self.line)
        if self.h_level:
            result = '<a class="heading" href="#%s">¶</a></h%d><p>\n' % (link, self.h_level)
            self.h_level = 0
        else:
            self.h_level = len(word) - 1
            result = '\n</p><h%d id="%s">' % (self.h_level, link)
        return result

    def _br_repl(self, word):
        return '<br />'

    def _rule_repl(self, word):
        return '\n<hr size="%d" noshade="noshade" />\n' % (len(word) - 2)

    def _macro_repl(self, word):
        try:
            args, macro_kvargs = parse_args(word)
            # Is this a parameter given to the current page?
            if args[0] in self.kvargs:
                return self.kvargs[args[0]]
            # Is this an internal macro?
            macro = globals().get('_macro_' + args[0])
            if not macro:
                # Can we load (and cache) an external macro?
                exec(open("sys/macros/" + args[0] + ".py").read(), globals())
                macro = globals().get('_macro_' + args[0])
            # Invoke macro passing both macro args augmented by page args
            macro_kvargs.update(self.kvargs)
            return macro(*args, **macro_kvargs)
        except Exception, e:
            msg = cgi.escape(word) + ": " + cgi.escape(str(e))
            if not self.in_html:
                msg = '<strong class="error">' + msg + '</strong>'
            return msg

    def _hurl_repl(self, word):
        args, kvargs = parse_args(word)
        return link_tag(*args, **kvargs)

    def _inl_repl(self, word):
        args, kvargs = parse_args(word)
        name = args.pop(0)
        if len(args):
            descr = args.pop(0)
            # This double div nonsense works around a limitation of the HTML block model
            return '<div class="' + kvargs.get('class', 'thumb') + '">' \
                + '<div class="innerthumb">' \
                + link_inline_glob(name, descr, kvargs) \
                + '<div class="caption">' + descr + '</div></div></div>'
        else:
            return link_inline_glob(name, None, kvargs)

    def _html_repl(self, word):
        if not self.in_html and word.startswith('<div'): word = '</p>' + word
        self.in_html += 1
        return word; # Pass through

    def _htmle_repl(self, word):
        self.in_html -= 1
        if not self.in_html and word.startswith('</div'): word += '<p>'
        return word; # Pass through

    def _ent_repl(self, s):
        if self.in_html:
            return s; # Pass through
        return {'&': '&amp;',
                '<': '&lt;',
                '>': '&gt;'}[s]

    def _img_repl(self, word): # LEGACY
        return self._inl_repl('{{' + word + '}}')

    def _word_repl(self, word): # LEGACY
        if self.in_html: return word # pass through
        return link_tag(word)

    def _url_repl(self, word): # LEGACY
        if self.in_html: return word # pass through
        return link_tag(word)

    def _email_repl(self, word): # LEGACY
        if self.in_html: return word # pass through
        return '<a href="mailto:%s">%s</a>' % (word, word)

    def _li_repl(self, match):
        if self.in_li:
            return '</li><li>'
        else:
            self.in_li = True
            return '<li>'

    def _pre_repl(self, word):
        if word == '{{{' and not self.in_pre:
            self.in_pre = True
            return '<pre>'
        elif self.in_pre:
            self.in_pre = False
            return '</pre>'
        return ''

    def _hi_repl(self, word):
        return '<strong class="highlight ' + word + '">' + word + '</strong>'

    def _tr_repl(self, word):
        out = ''
        if not self.in_table:
            self.in_table = True
            self.tr_cnt = 0
            out = '</p><table><tbody>\n'
        self.tr_cnt += 1
        out = out + '<tr class="' + ['even', 'odd'][self.tr_cnt % 2] + '">'
        return out + ['<td>', '<th>'][word.strip() == '||=']

    def _td_repl(self, word):
        if self.in_table:
            return ['</td><td>', '</th><th>'][word.strip() == '||=']
        return ''

    def _tre_repl(self, word):
        if self.in_table:
            return ['</td></tr>', '</th></tr>'][word.strip() == '||=']
        return ''

    def _indent_level(self):
        return len(self.list_indents) and self.list_indents[-1][0]

    def _indent_to(self, new_level, list_type=''):
        if self._indent_level() == new_level:
            return ''
        s = '</p>'
        while self._indent_level() > new_level:
            if self.in_li:
                s += '</li>'
                self.in_li = False # FIXME
            s += '</' + self.list_indents[-1][1] + '>\n'
            del(self.list_indents[-1])

        list_type = ('ul', 'ol')[list_type == '#']
        while self._indent_level() < new_level:
            self.list_indents.append((new_level, list_type))
            s += '<' + list_type + '>\n'
        s += '<p>'
        return s

    def replace(self, match):
        for rule, hit in list(match.groupdict().items()):
            if hit:
                return getattr(self, '_' + rule + '_repl')(hit)
        else:
            raise Exception("Can't handle match " + repr(match))

    def print_html(self):
        print('<div class="wiki"><p>')

        scan_re = re.compile(r"""(?:
            # Styles and formatting ("--" must cling to a word to disambiguate it from the dash)
              (?P<b>     \*\* | // | \#\# | __ | --\b | \b-- | \^\^ | ,, | ''' | '' )
            | (?P<tit>   \={2,6})
            | (?P<br>    \\\\)
            | (?P<rule>  ^-{3,})
            | (?P<hi>    \b( FIXME | TODO | DONE )\b )
            | (?P<glyph> --)

            # Links
            | (?P<macro> \<\<[^\>]+\>\>)
            | (?P<hurl>  \[\[[^\]]+\]\])

            # Inline HTML
            | (?P<html>             <(br|hr|small|div|span|form|iframe|input|textarea|a|img|h[1-5])\b )
            | (?P<htmle> ( /\s*> | </(br|hr|small|div|span|form|iframe|input|textarea|a|img|h[1-5])> ) )
            | (?P<ent>   [<>&] )

            # Auto links (LEGACY)
            | (?P<img>   \b[a-zA-Z0-9_/-]+\.(""" + image_ext + "|" + video_ext + r"""))
            | (?P<word>  \b(?:[A-Z][a-z]+){2,}\b)
            | (?P<url>   (http|https|ftp|mailto)\:[^\s'\"]+\S)
            | (?P<email> [-\w._+]+\@[\w.-]+)

            # Lists, divs, spans and inline objects
            | (?P<li>    ^\s+[\*\#]\s+)
            | (?P<pre>   \{\{\{|\s*\}\}\})
            | (?P<inl>   \{\{[^\}]+\}\})

            # Tables
            | (?P<tr>    ^\s*\|\|(=|)\s*)
            | (?P<tre>   \s*\|\|(=|)\s*$)
            | (?P<td>    \s*\|\|(=|)\s*)
          )""", re.VERBOSE)
        pre_re = re.compile("""(?:
              (?P<pre>\s*\}\}\})
            | (?P<ent>[<>&])"
            )""", re.VERBOSE)
        blank_re = re.compile(r"^\s*$")
        indent_re = re.compile(r"^(\s*)(\*|\#|)")
        tr_re = re.compile(r"^\s*\|\|")
        eol_re = re.compile(r"\r?\n")
        # For each line, we scan through looking for magic strings, outputting verbatim any intervening text
        #3.0: for self.line in eol_re.split(str(self.raw.expandtabs(), 'utf-8')):
        for self.line in eol_re.split(str(self.raw.expandtabs())):
            # Skip pragmas
            if self.in_header:
                if self.line.startswith('#'):
                    continue
                self.in_header = False

            if self.in_pre:
                print(re.sub(pre_re, self.replace, self.line))
            else:
                if self.in_table and not tr_re.match(self.line):
                    self.in_table = False
                    print('</tbody></table><p>')

                if blank_re.match(self.line):
                    print('</p><p>')
                else:
                    indent = indent_re.match(self.line)
                    print(self._indent_to(len(indent.group(1)), indent.group(2)))
                    # Stand back! Here we apply the monster regex that does all the parsing
                    print(re.sub(scan_re, self.replace, self.line))

        if self.in_pre: print('</pre>')
        if self.in_table: print('</tbody></table><p>')
        print(self._indent_to(0))
        print('</p></div>')

class HttpException(Exception):
    def __init__(self, error, query):
        self.error = error
        self.query = query

class Page:
    def __init__(self, page_name="Limbo"):
        self.page_name = page_name.rstrip('/');
        self.msg_text = ''
        self.msg_type = 'error'
        if not file_re.match(self.page_name):
            raise HttpException("403 Forbidden", self.page_name)

    def split_title(self):
        # look for the end of words and the start of a new word and insert a space there
        return re.sub('([a-z])([A-Z])', r'\1 \2', self.page_name)

    def _filename(self):
        return self.page_name

    def _tmp_filename(self):
        return self.page_name + '.tmp' + str(os.getpid()) + '#'

    def _mtime(self):
        try:
            return os.stat(self._filename()).st_mtime
        except OSError, err:
            if err.errno == errno.ENOENT:
                return None
            raise err

    def exists(self):
        if self._mtime():
            return True
        return False

    def get_raw_body(self):
        try:
            return open(self._filename(), 'rb').read()
        except IOError, err:
            if err.errno == errno.ENOENT:
                return ''
            if err.errno == errno.EISDIR:
                return self.format_dir()
            raise err

    def format_dir(self):
        out = '== '
        pathname = ''
        for dirname in self.page_name.strip('/').split('/'):
            pathname = (pathname and pathname + '/' ) + dirname
            out += '[[' + pathname + '|' + dirname + ']]/'
        out += ' ==\n'
        images_out = '\n'

        for filename in page_list(self._filename(), file_re):
            if image_re.match(filename) or video_re.match(filename):
                maxwidth = config_get('image_maxwidth', '400')
                if maxwidth:
                    maxwidth = ' | maxwidth=' + str(maxwidth)
                images_out += '{{' + self.page_name + '/' + filename + ' | ' + humanlink(filename) + maxwidth + ' | class=thumbleft}}\n'
            else:
                out += ' * [[' + self.page_name + '/' + filename + ']]\n'
        return out + images_out

    def pragmas(self):
        if not '_pragmas' in self.__dict__:
            self._pragmas = {}
            try:
                file = open(self._filename(), 'rt')
                attr_re = re.compile(r"^#(\S*)(.*)$")
                for line in file:
                    m = attr_re.match(line)
                    if not m:
                        break
                    self._pragmas[m.group(1)] = m.group(2).strip()
                    #print "bernie: pragmas[" + m.group(1) + "] = " + m.group(2) + "<br>\n"
            except IOError, err:
                if err.errno != errno.ENOENT and err.errno != errno.EISDIR:
                    raise err
        return self._pragmas

    def pragma(self, name, default):
        return self.pragmas().get(name, default)

    def can(self, action, default=True):
        acl = None
        try:
            #acl SomeUser:read,write All:read
            acl = self.pragma("acl", None)
            for rule in acl.split():
                (user, perms) = rule.split(':')
                if user == remote_user() or user == "All":
                    return action in perms.split(',')
            return False
        except Exception:
            if acl:
                self.msg_text = 'Illegal acl line: ' + acl
        return default

    def can_write(self):
        return self.can("write", True)

    def can_read(self):
        return self.can("read", True)

    def send_title(self, name=None, text="Limbo", msg_text=None, msg_type='error'):
        global title_done
        if title_done: return

        # HEAD
        emit_header(name and self._mtime())
        print('<!doctype html>\n<html lang="en">')
        print("<head><title>%s: %s</title>" % (config_get('site_name', "Unconfigured Wiki"), text))
        print(' <meta charset="utf-8">')
        print(' <meta name="viewport" content="width=device-width, initial-scale=1.0">')
        if not name:
            print(' <meta name="robots" content="noindex,nofollow" />')

        for http_equiv, content in config_get('meta_urls', {}):
            print(' <meta http-equiv="%s" content="%s" />' % (http_equiv, relative_url(content)))

        for link in config_get('link_urls', {}):
            rel, href = link
            print(' <link rel="%s" href="%s" />' % (rel, relative_url(href)))

        editable = name and self.can_write() and is_privileged()
        if editable:
            print(' <link rel="alternate" type="application/x-wiki" title="Edit this page" href="%s" />' \
                % relative_url(name + '?a=edit', privileged=True))

        if name:
            print(' <link rel="alternate" type="application/atom+xml" title="Atom feed" href="%s" />' \
                % relative_url(name + '?a=atom'))

        print('</head>')

        # BODY
        if editable:
            print('<body ondblclick="location.href=\'' + relative_url(name + '?a=edit', privileged=True) + '\'">')
        else:
            print('<body>')

        title_done = True
        send_guru(msg_text, msg_type)

        if self.pragma("navbar", "on") != "on":
            return

        # NAVBAR
        print('<nav><div class="nav">')
        print link_tag('FrontPage', config_get('site_icon', 'Home'), cssclass='navlink')
        if name:
            print('  <b>' + link_tag('?a=titlesearch&q=' + name, text, cssclass='navlink') + '</b> ')
        else:
            print('  <b>' + text + '</b> ')
        print(' | ' + link_tag('FindPage', 'Find Page', cssclass='navlink'))
        history = config_get('history_url')
        if history:
            print(' | <a href="' + relative_url(history) + '" class="navlink">Recent Changes</a>')
            if name:
                print(' | <a href="' + relative_url(history + '?a=history;f=' + name) + '" class="navlink">Page History</a>')

        if name:
            print(' | ' + link_tag(name + '?a=raw', 'Raw Text', cssclass='navlink'))
            if config_get('privileged_url') is not None:
                if self.can_write():
                    print(' | ' + link_tag(name + '?a=edit', 'Edit', cssclass='navlink', privileged=True))
                else:
                    print(' | ' + link_tag(name, 'Login', cssclass='login', privileged=True))

        user = remote_user()
        if user != 'AnonymousCoward':
            print(' | ' + link_tag('user/' + user, user, cssclass='login'))

        print('<hr /></div></nav>')

    def send_footer(self):
        if config_get('debug_cgi', False):
            cgi.print_arguments()
            cgi.print_form(form)
            cgi.print_environ()
        footer = self.pragma("footer", "sys/footer")
        if footer != "off":
            link_inline(footer, kvargs = {
                'LAST_MODIFIED': strftime(config_get('datetime_fmt', '%Y-%m-%dT%I:%M:%S%p'), localtime(self._mtime()))
            })
        print('</body></html>')

    def send_naked(self, kvargs=None):
        if self.can_read():
            body = self.get_raw_body()
            if not body:
                body = "//[[%s?a=edit|Describe %s]]//" % (self.page_name, self.page_name)
            WikiFormatter(body, kvargs).print_html()
        else:
            send_guru('Read access denied by ACLs', 'notice')

    def send(self):
        #css foo.css
        value = self.pragma("css", None)
        if value:
            global link_urls
            link_urls += [ [ "stylesheet", value ] ]

        self.send_title(name=self.page_name, text=self.split_title(), msg_text=self.msg_text, msg_type=self.msg_type)
        self.send_naked()
        self.send_footer()

    def send_atom(self):
        emit_header(self._mtime(), 'application/atom+xml')
        self.in_html = True
        link_inline("sys/atom_header", kvargs = {
            'LAST_MODIFIED': strftime(config_get('datetime_fmt', '%a, %d %b %Y %I:%M:%S %p'), localtime(self._mtime()))
        })
        self.in_html = False
        self.send_naked()
        self.in_html = True
        link_inline("sys/atom_footer")
        self.in_html = False

    def send_editor(self, preview=None):
        self.send_title(text='Edit ' + self.split_title(), msg_text=self.msg_text, msg_type=self.msg_type)
        if not self.can_write():
            send_guru("Write access denied by ACLs", "error")
            return

        if preview is None:
            preview = self.get_raw_body()

        link_inline("sys/EditPage", kvargs = {
            'EDIT_BODY': cgi.escape(preview),
            #'EDIT_PREVIEW': WikiFormatter(preview).print_html(),
        })

        if preview:
            print("<div class='preview'>")
            WikiFormatter(preview).print_html()
            print("</div>")
        self.send_footer()

    def send_raw(self, mimetype='text/plain', args=[]):
        if not self.can_read():
            self.send_title(msg_text='Read access denied by ACLs', msg_type='notice')
            return

        emit_header(self._mtime(), mimetype)
        if 'maxwidth' in args:
            import subprocess
            sys.stdout.flush()
            subprocess.check_call(['convert', self._filename(),
                '-auto-orient', '-orient', 'TopLeft',
                '-scale', args['maxwidth'].value + ' >', '-'])
        else:
            body = self.get_raw_body()
            print(body)

    def _write_file(self, data):
        tmp_filename = self._tmp_filename()
        open(tmp_filename, 'wb').write(data)
        name = self._filename()
        if os.name == 'nt':
            # Bad Bill!  POSIX rename ought to replace. :-(
            try:
                os.remove(name)
            except OSError, err:
                if err.errno != errno.ENOENT: raise err
        path = os.path.split(name)[0]
        if path and not os.path.exists(path):
            os.makedirs(path)
        os.rename(tmp_filename, name)

    def save(self, newdata, changelog):
        if not self.can_write():
            self.msg_text = 'Write access denied by Access Control List'
            return
        if not is_privileged():
            self.msg_text = 'Unauthenticated access denied'
            return

        self._write_file(newdata)
        rc = 0
        if config_get('post_edit_hook'):
            import subprocess
            cmd = [
                config_get('post_edit_hook'),
                self.page_name, remote_user(),
                remote_host(), changelog ]
            child = subprocess.Popen(cmd, stdout=subprocess.PIPE, close_fds=True)
            output = child.stdout.read()
            rc = child.wait()
        if rc:
            self.msg_text += "Post-editing hook returned %d. Command was:\n'%s'\n" % (rc, "' '".join(cmd))
            if output:
                self.msg_text += 'Output follows:\n' + output
        else:
            self.msg_text = 'Thank you for your contribution.  Your attention to detail is appreciated.'
            self.msg_type = 'success'

try:
    exec(open("geekigeeki.conf.py").read())
    os.chdir(config_get('data_dir', 'data'))
    form = cgi.FieldStorage()
    action = form.getvalue('a', 'get')
    handler = globals().get('handle_' + action)
    if handler:
        handler(query_string(), form)
    else:
        send_httperror("403 Forbidden", query_string())

except HttpException, e:
    send_httperror(e.error, query=e.query)
except Exception:
    send_httperror("500 Internal Server Error", query=query_string(), trace=True)

sys.stdout.flush()
